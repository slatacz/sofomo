import { useEffect, useState } from 'react';
import SearchList from './components/SearchList'
import RowComponent from './components/RowComponent';
import InputComponent from './components/InputComponent';
import Loader from './components/Loader';

import 'mapbox-gl/dist/mapbox-gl.css';
import './App.css';


function App() {
  const [history, setHistory] = useState([]);
  const [currentLocation, setCurrentLocation] = useState(undefined)
  const [searchedLocation, setSearchedLocation] = useState(undefined)
  const [inputValue, setInputValue] = useState('')
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(undefined)

  // data fetching
  const fetcher  = (urlString, initial) => (
    fetch(`http://api.ipstack.com/${urlString}?access_key=${process.env.REACT_APP_API_KEY}`)
      .then(response => response.json())
      .then(data => {
        if(!data.error){
          if(initial){
            setCurrentLocation(data)
          }else{
            setHistory(prev => [...prev, data])
            setSearchedLocation(data)
          }
          setLoading(false)
        }else setError({message: data.error.info, source: initial? 'user': 'input'})
      })
  )

  const fetchData = () => {
    setLoading(true)
    if(currentLocation) {
      if(inputValue === '') {
        setError({message: `Input can't be empty`, source: 'input'})
        return
      }
      fetcher(inputValue, false)
    }
    else{
      fetch('https://geolocation-db.com/json/')
      .then(response => {
        if(response.ok) return response.json()
        else setError({message: `Couldn't get your location`, source: 'user'})
      })
      .then(data => {
        fetcher(data.IPv4, true)
      })
      .catch(err => console.log(err))
    }
  }

  // user actions handlers
  const onChange = (e) => {
    setInputValue(e.target.value)
  }

  const handleSettingLocation = (item) =>{
    setError(undefined)
    setSearchedLocation(item)
  }

  useEffect(() => {
    fetchData()
  },[])

  useEffect(() => {
    if(!error) return
    else {
      setSearchedLocation(undefined)
      setLoading(false)
    }
  },[error])

  return (
    <div className="container">
      {(!loading || currentLocation)? 
        <>
          <SearchList 
            history={history}
            setSearchedLocation={handleSettingLocation}
          />
          <div className='main'>
            {currentLocation&& 
              <RowComponent 
                currentLocation={currentLocation}
                error= {error?.source}
                type = "home"
                message="Can't access current location"
              />
            }
            <InputComponent 
              fetchData={fetchData}
              setError={setError}
              loading={loading}
              error={error}
              onChange={onChange}
            />
            {(searchedLocation && !error)&&
              <RowComponent 
                currentLocation = {searchedLocation}
                type = "target"
                message = "Can't access current location"
              />
            }
          </div>
        </>
        : <Loader/>
      }
    </div>
  );
}

export default App;
