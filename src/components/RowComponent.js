import Map, {Marker} from 'react-map-gl';

const RowComponent = (props) => {
  const {currentLocation, message, error} = props

  const initialState = {
    zoom: 12,
    longitude: currentLocation.longitude,
    latitude: currentLocation.latitude,
  }

  return (
    <div className='row'>
      <div className='map-location'>
        {(error&& error === 'user')? <img className='error-image' src='https://i.ibb.co/dKSg0QR/error.jpg' alt='error' />:
          <Map
            {...initialState}
            mapStyle="mapbox://styles/mapbox/streets-v9"
            mapboxAccessToken={process.env.REACT_APP_MAPBOX_API_KEY}
          >
            {currentLocation?
              <Marker longitude={currentLocation.longitude} latitude={currentLocation.latitude} anchor="bottom" >
                <svg className='marker' xmlns="http://www.w3.org/2000/svg " viewBox="0 0 24 24">
                  <path d="M12 0c-5.522 0-10 4.395-10 9.815 0 5.505 4.375 9.268 10 14.185 5.625-4.917 10-8.68 10-14.185 0-5.42-4.478-9.815-10-9.815zm0 18c-4.419 0-8-3.582-8-8s3.581-8 8-8 8 3.582 8 8-3.581 8-8 8z"/>
                </svg> 
              </Marker>
              :
              <div className='empty'>{message}</div>
            }
          </Map>
        }
      </div>
      <div className='more-info'>
        <h2>Location Details</h2>
        {currentLocation?
          <div className='properties-container'>
            {Object.keys(currentLocation).map((key, idx) => (
              <>
                {key !== 'location' && <div className='property' key={`item-${idx}`}>{key}: {currentLocation[key]}</div>}
              </>
            ))
            }
          </div>
          :
          <div className='empty'>{message}</div>
        } 
      </div>
    </div>
  )
}

export default RowComponent