const SearchList = (props) => {
  const {history, setSearchedLocation} = props
  return (
    <div className='search-list'>
      {history.length < 1?
        <div className='empty'>No previous search history </div>
        :
        <div className='history-container'>
          {history.map(item => (
            <div className='history-item' onClick={() => setSearchedLocation(item)}>{item.ip}</div>
          ))}
        </div>
      }
    </div>
  )
}

export default SearchList