const InputComponent = (props) => {
  const {fetchData, setError, loading, error, onChange} = props
  return(
    <div className='row'>
      <div className='padding'>
        <div className="control">
          <input type='text' placeholder='Enter IP address or URL' onFocus={() => setError(undefined)} onChange={onChange} />
          <button onClick={fetchData} disabled={loading}>Search</button>
        </div>
        {error && <div className='error'>{error.message}</div>}
      </div>
    </div>
  )
}

export default InputComponent